#!/usr/bin/python2.7
import shellconnect
import mysqlconnect
import subprocess
from logger import Logger

class remotehosts():

    def __init__(self):
        self.logger = Logger(self.__class__.__name__).get()

    def checkhostname(self, hostname):
        mysql = mysqlconnect.mysqlconnect()
        result = mysql.sendquery("SELECT * FROM remotehosts WHERE name = '%s'" % hostname)
        mysql.closeconnection()
        if result:
            return True
        else:
            return False

    def checkip(self, IP):
        mysql = mysqlconnect.mysqlconnect()
        result = mysql.sendquery("SELECT * FROM remotehosts WHERE IP = '%s'" % IP)
        mysql.closeconnection()
        if result:
            return True
        else:
            return False

    def addhost(self, hostname, IP, username, password):
        if self.checkhostname(hostname) == False and self.checkip(IP) == False:
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("INSERT INTO remotehosts (ID, name, IP, user, pass) VALUES (0, '%s', '%s', '%s', '%s')" % (hostname, IP, username, password))
            mysql.closeconnection()
            self.logger.info("Added Remotehost -> " + hostname + "/" + IP)
        else:
            self.logger.error("Hostname or IP conflict detected.")

    def removehost(self, IP):
        mysql = mysqlconnect.mysqlconnect()
        mysql.sendinsert("DELETE FROM remotehosts WHERE IP = '%s'" % IP)
        mysql.closeconnection()
        self.logger.info("Removed Remotehost -> " + IP)