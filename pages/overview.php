<div id="rightside">

<div class="jumbotron" style="padding: 5px !important;">
<h2>Users</h2>

<table class="table table-hover">
  <thead>
    <tr style="background-color: #505050;">
      <th scope="col">Username</th>
      <th scope="col">Group Membership</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  <tbody>
<?php
$query = "SELECT * FROM user";
$result = $mysqli->query ($query);
                
$i = 0;
while ($row = $result->fetch_assoc()) {
	if($i<$result->num_rows) {
        $username[$i] = $row["name"];
        $state[$i] = $row["state"];
                            
        $i++;
    }
}

for ($i = 0; $i<sizeof($username); $i++){
	if ($state[$i] == "active"){
		echo "<tr><th scope=\"row\">".$username[$i]."</th>";
	}
	elseif ($state[$i] == "locked") {
		echo "<tr class=\"table-warning\"><th scope=\"row\">".$username[$i]."</th>";
	}
	else{
		echo "<tr class=\"table-danger\"><th scope=\"row\">".$username[$i]."</th>";
	}
	echo "<td>";

	$query = "SELECT groupname FROM mapping WHERE username = '$username[$i]';";
	$result = $mysqli->query ($query);

           
	$iter = 0;
	while ($row2 = $result->fetch_assoc()) {
		if($iter<$result->num_rows) {
        	$groupmember[$iter] = $row2["groupname"];
                            
        	$iter++;
    	}
	}

	for ($iter = 0; $iter<sizeof($groupmember);$iter++){
		if(isset($groupmember[$iter])){
			echo " »".$groupmember[$iter];
		}
	}

	$groupmember = "";

	echo "</td>";
	echo "<td>".$state[$i]."</td></tr>";
}
?>
  </tbody>
</table> 
</div>


<div class="jumbotron" style="padding: 5px !important;">
<h2>Groups</h2>

<table class="table table-hover">
  <thead>
    <tr style="background-color: #505050;">
      <th scope="col">Groupname</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  <tbody>
<?php
$query = "SELECT * FROM groups";
$result = $mysqli->query ($query);
                
$i = 0;
while ($row = $result->fetch_assoc()) {
	if($i<$result->num_rows) {
        $groupname[$i] = $row["name"];
        $state[$i] = $row["state"];
                            
        $i++;
    }
}

for ($i = 0; $i<sizeof($groupname); $i++){
	if ($state[$i] == "active"){
		echo "<tr><th scope=\"row\">".$groupname[$i]."</th><td>".$state[$i]."</td></tr>";
	}
	else{
		echo "<tr class=\"table-danger\"><th scope=\"row\">".$groupname[$i]."</th><td>".$state[$i]."</td></tr>";
	}
}
?>
  </tbody>
</table> 
</div>

<div class="jumbotron" style="padding: 5px !important;">
<h2>Remotehosts</h2>

<table class="table table-hover">
  <thead>
    <tr style="background-color: #505050;">
      <th scope="col">Hostname</th>
      <th scope="col">IP</th>
    </tr>
  </thead>
  <tbody>
    
<?php
$query = "SELECT * FROM remotehosts";
$result = $mysqli->query ($query);
                
$i = 0;
while ($row = $result->fetch_assoc()) {
  if($i<$result->num_rows) {
        $hostname[$i] = $row["name"];
        $ip[$i] = $row["IP"];
        $user[$i] = $row["user"];
        $pass[$i] = $row["pass"];
                            
        $i++;
    }
}

function arrayToString($out){
    return implode("<br>", $out);
}

for ($i = 0; $i<sizeof($hostname); $i++){
  $cmd = "/var/www/html/usermgmt/py/start.py --check-host-is-up ".$ip[$i]." ".$user[$i]." ".$pass[$i]."";
  exec($cmd, $output);
  if(arrayToString($output) != ""){
   echo "<tr><th scope=\"row\" style=\"color: crimson;\">".$hostname[$i]."<div class=\"wake\" onclick=\"location.href='api.php?on&name=".$hostname[$i]."'\">WoL</div></th><td>".$ip[$i]."</td></tr>";
   $output = "";
  }
  else{
    echo "<tr><th scope=\"row\" style=\"color: green;\">".$hostname[$i]."</th><td>".$ip[$i]."</td></tr>";
  }
}
?>

  </tbody>
</table> 
</div>

</div>
