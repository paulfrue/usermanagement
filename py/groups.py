#!/usr/bin/python2.7
import mysqlconnect
from logger import Logger
import user

class groups():

    def __init__(self):
        self.logger = Logger(self.__class__.__name__).get()

    def checkgroupexists(self, groupname):
        mysql = mysqlconnect.mysqlconnect()
        result = mysql.sendquery("SELECT * FROM groups WHERE name = '%s'" % groupname)
        mysql.closeconnection()
        if result:
            return True
        else:
            return False

    def creategroup(self, groupname):
        if self.checkgroupexists(groupname):
            self.logger.error("Group -> " + groupname + " already exists.")
        else:
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("INSERT INTO groups (ID,name,state) VALUES (0, '%s', 'active')" % groupname)
            mysql.closeconnection()
            self.logger.info("Created Group -> " + groupname)

    def removegroup(self, groupname):
        if self.checkgroupexists(groupname):
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("UPDATE groups SET state = 'deleted' WHERE name = '%s'" % groupname)
            mysql.closeconnection()
            self.logger.info("Removed Group -> " + groupname)
        else:
            self.logger.error("Group -> " + groupname + " doesn't exist.")

    def addusertogroup(self, username, groupname):
        obj = user.user()
        if user.user.checkuserexists(obj, username) == True and self.checkgroupexists(groupname) == True:
            mysql = mysqlconnect.mysqlconnect()
            result = mysql.sendquery("SELECT * FROM mapping WHERE username = '%s' AND groupname = '%s'" % (username, groupname))
            mysql.closeconnection()
            if result:
                self.logger.error("User %s is already a member of %s." % (username, groupname))
            else:
                mysql = mysqlconnect.mysqlconnect()
                mysql.sendinsert("INSERT INTO mapping (ID,username,groupname) VALUES (0, '%s', '%s')" % (username, groupname))
                mysql.closeconnection()
                self.logger.info("User -> " + username + " is now a member of -> " + groupname)
        else:
            self.logger.error("User or Group doesn't exist.")

    def removeuserfromgroup(self, username, groupname):
        obj = user.user()
        if user.user.checkuserexists(obj, username) == True and self.checkgroupexists(groupname) == True:
            mysql = mysqlconnect.mysqlconnect()
            result = mysql.sendquery("SELECT * FROM mapping WHERE username = '%s' AND groupname = '%s'" % (username, groupname))
            mysql.closeconnection()
            if result:
                mysql = mysqlconnect.mysqlconnect()
                mysql.sendinsert("DELETE FROM mapping WHERE username = '%s' AND groupname = '%s'" % (username, groupname))
                mysql.closeconnection()
                self.logger.info("User -> " + username + " is no longer a member of -> " + groupname)
            else:
                self.logger.error("User %s is not a member of %s." % (username, groupname))
        else:
            self.logger.error("User or Group doesn't exist.")
