<div id="rightside">

<form action="api.php?h=1" method="post">
<div class="jumbotron" style="padding: 5px !important;">

<h2>Add Remotehost</h2>

<div class="form-group">
  <label class="col-form-label" for="inputDefault">Hostname</label>
  <input name="hostname" type="text" class="form-control" placeholder="DNS-Name" id="inputDefault">
</div>

<div class="form-group">
  <label class="col-form-label" for="inputDefault">IP</label>
  <input name="ip" type="text" class="form-control" placeholder="0.0.0.0" id="inputDefault">
</div>

<div class="form-group">
  <label class="col-form-label" for="inputDefault">Login User</label>
  <input name="username" type="text" class="form-control" placeholder="root" id="inputDefault">
</div>

<div class="form-group">
  <label for="exampleInputPassword1">Login Password</label>
  <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="***">
</div>

<button type="submit" class="btn btn-primary">Submit</button>

</div>
</form>




<form action="api.php?h=2" method="post">
<div class="jumbotron" style="padding: 5px !important;">

<h2>Remove Remotehost</h2>

<div class="form-group">
      <label for="exampleSelect1">Hostname / IP</label>
      <select name="ip" class="form-control" id="exampleSelect1">

<?php
$query = "SELECT * FROM remotehosts";
$result = $mysqli->query ($query);
                
$i = 0;
while ($row = $result->fetch_assoc()) {
  if($i<$result->num_rows) {
        echo "<option value=\"".$row["IP"]."\">".$row["name"]." / ".$row["IP"]."</option>";
                            
        $i++;
    }
}
?>
  </select>
</div>


<button type="submit" class="btn btn-primary">Submit</button>

</div>
</form>

</div>
