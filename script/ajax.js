var ajaxObjProgress = null;
    ajaxObjProgress = new XMLHttpRequest();

var ajaxObjStatusMessage = null;
    ajaxObjStatusMessage = new XMLHttpRequest();

function progressrefresh() {
    if (ajaxObjProgress.readyState == 4) {
        var progress = ajaxObjProgress.responseText;
    }
    if(progress != undefined){
        document.getElementById("inner_bar").setAttribute("style", "width: "+progress*7+"px");
    }
}

function statusmessagerefresh() {
    if (ajaxObjStatusMessage.readyState == 4) {
        var message = ajaxObjStatusMessage.responseText;
    }
    if(message != undefined){
        document.getElementById("statusmessage").innerHTML = message;
    }
}

function ajaxloop(){
    ajaxObjProgress.open("GET", "progress.php");
    ajaxObjProgress.onreadystatechange = progressrefresh;
    ajaxObjProgress.send(null);
    ajaxObjStatusMessage.open("GET", "statusmessage.php");
    ajaxObjStatusMessage.onreadystatechange = statusmessagerefresh;
    ajaxObjStatusMessage.send(null);
    setTimeout(function(){
        ajaxloop();
    }, 500);
}

function init(bar){
    if(bar){
        document.getElementById('statusmessage').setAttribute("style", "display: block;");
        document.getElementById('outer_bar').setAttribute("style", "display: block;");
        document.getElementById('overlay').setAttribute("style", "display: block;");
    }
    else{
        document.getElementById('overlay').setAttribute("style", "display: block; background-color: rgba(0,0,0,0.8);");
    }
}