#!/usr/bin/python2.7
import shellconnect
import mysqlconnect
from tqdm import tqdm
import subprocess
import time
import settings

class deploy():

	def __init__(self):
		self.actionCounter = 0
		self.actionTotal = 0
		self.controllerIP = settings.CONTROLLER_IP
		self.bootDelaySeconds = settings.BOOT_DELAY
		self.bootDelay = False

	def synchronizeHosts(self):
		mysql = mysqlconnect.mysqlconnect()
		hostresult = mysql.getAllHosts()
		hostsize = mysql.countrow("SELECT * FROM remotehosts")
		mysql.closeconnection()

		if hostresult:
			for hostrow in hostresult:
				self.startHost(hostrow["name"])
				time.sleep(1)

			if self.bootDelay: 
				while self.bootDelaySeconds != 0:
					self.writeStatusMessage("Waiting for hosts to boot... "+str(self.bootDelaySeconds))
					time.sleep(1)
					subprocess.call(['clear'])
					self.bootDelaySeconds = self.bootDelaySeconds - 1

			for hostrow in hostresult:
				hostOnline = self.checkHostIsUp(hostrow["IP"], hostrow["user"], hostrow["pass"])
				if hostOnline == True:
					self.synchronizeUsersOnHost(hostrow)
					self.synchronizeGroupsOnHost(hostrow)
				else:
					skipvalue = self.actionTotal/hostsize
					self.actionCounter = self.actionCounter+skipvalue
					self.writeProgress(self.actionCounter)

		self.synchronizeDatabase()


	def synchronizeUsersOnHost(self, host):
		mysql = mysqlconnect.mysqlconnect()
		userresult = mysql.getAllUsers()
		mysql.closeconnection()

		self.writeStatusMessage("Synchronize User on " + host["name"])
		
		for userrow in tqdm((userresult), ncols=80, desc="Progress"):
			userexists = self.checkUserExists(userrow["name"], host["IP"], host["user"], host["pass"])

			if(userrow["state"] == 'deleted'):
				if userexists == True:
					self.deleteUserOnHost(host, userrow)

			if(userrow["state"] == 'locked'):
				if userexists == False:
					self.createUserOnHost(host, userrow)
				self.lockUserOnHost(host, userrow)

			if(userrow["state"] == 'active'):
				if userexists == False:
					self.createUserOnHost(host, userrow)
				self.unlockUserOnHost(host, userrow)
				self.changeUserPasswordOnHost(host, userrow)
				if (userrow["pass"] == settings.DEFAULT_PASSWD):
					self.disablePasswordNotification(host, userrow)
					self.enablePasswordNotification(host, userrow)
				else:
					self.disablePasswordNotification(host, userrow)
				mappingresult = self.getMappingResultsFromUser(userrow)
				if (mappingresult):
					self.resetGroupMembershipOfUser(host, userrow)
					for mapping in mappingresult:
						self.addGroupToUser(host, mapping, userrow)

			self.actionCounter = self.actionCounter+1
			self.writeProgress(self.actionCounter)

	def synchronizeGroupsOnHost(self, host):
		mysql = mysqlconnect.mysqlconnect()
		groupresult = mysql.getAllGroups()
		mysql.closeconnection()

		self.writeStatusMessage("Synchronize Groups on " + host["name"])

		for grouprow in tqdm((groupresult), ncols=80, desc="Progress"):
			groupexists = self.checkGroupExists(grouprow["name"], host["IP"], host["user"], host["pass"])

			if(grouprow["state"] == 'active'):
				if groupexists == False:
					self.createGroupOnHost(grouprow, host)

			if(grouprow["state"] == 'deleted'):
				mappingresult = self.getMappingResultFromGroup(grouprow)
				for row in mappingresult:
					self.removeGroupFromUser(host, row)
				if groupexists == True:
					self.deleteGroupOnHost(grouprow, host)

			self.actionCounter = self.actionCounter+1
			self.writeProgress(self.actionCounter)

	def synchronizeDatabase(self):
		mysql = mysqlconnect.mysqlconnect()
		userresult = mysql.getAllUsers()
		groupresult = mysql.getAllGroups()
		mysql.closeconnection()

		self.writeStatusMessage("Synchronize Group Database")

		for grouprow in tqdm((groupresult), ncols=82, desc="Progress"):
			if(grouprow["state"] == 'deleted'):
				self.deleteGroupAndMappingInDB(grouprow)

		time.sleep(1)

		self.writeStatusMessage("Synchronize User Database")

		for userrow in tqdm((userresult), ncols=81, desc="Progress"):
			if(userrow["state"] == 'deleted'):
				self.deleteUserAndMappingInDB(userrow)

		time.sleep(1)

#-----------------------User Methods-------------------------

	def checkUserExists(self, username, host, hostuser, hostpass):
		cmd = 'sudo cat /etc/passwd | grep %s' % username
		shell = shellconnect.shellconnect(host, hostuser, hostpass)
		output = shell.sendcommand(cmd)
		if output == "False" or output == "":
			return False
		else:
			return True

	def createUserOnHost(self, hostrow, userrow):
		cmd = 'sudo useradd -s /bin/bash -N -m %s' % userrow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def deleteUserOnHost(self, hostrow, userrow):
		cmd = 'sudo deluser --remove-all-files %s && sudo rm -r -f /home/%s' % (userrow["name"], userrow["name"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def changeUserPasswordOnHost(self, hostrow, userrow):
		cmd = 'echo "%s:%s" > /etc/userpass.tmp && chpasswd -m < /etc/userpass.tmp && rm /etc/userpass.tmp' % (
			userrow["name"], userrow["pass"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def unlockUserOnHost(self, hostrow, userrow):
		cmd = 'sudo usermod -U %s' % userrow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def lockUserOnHost(self, hostrow, userrow):
		cmd = 'sudo usermod -L %s' % userrow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def enablePasswordNotification(self, hostrow, userrow):
		cmd = "echo notify-send \\'Warning\\' \\'Please visit http://%s/password.php?user=%s to change your Password.\\' >> /home/%s/.bashrc" % (self.controllerIP, userrow["name"], userrow["name"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def disablePasswordNotification(self, hostrow, userrow):
		cmd = 'grep -v notify-send /home/%s/.bashrc > /home/%s/temp && mv /home/%s/temp /home/%s/.bashrc' % (userrow["name"], userrow["name"], userrow["name"], userrow["name"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def resetGroupMembershipOfUser(self, hostrow, userrow):
		cmd = 'sudo usermod -G users %s' % userrow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		out = shell.sendcommand(cmd)

	def addGroupToUser(self, hostrow, mapping, userrow):
		cmd = 'sudo usermod -a -G %s %s' % (mapping["groupname"], userrow["name"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def getMappingResultsFromUser(self, userrow):
		mysql = mysqlconnect.mysqlconnect()
		result = mysql.sendquery("SELECT * FROM mapping WHERE username = '%s'" % userrow["name"])
		mysql.closeconnection()
		return result

	def deleteUserAndMappingInDB(self, userrow):
		mysql = mysqlconnect.mysqlconnect()
		mysql.sendinsert("DELETE FROM user WHERE name = '%s'" % userrow["name"])
		mysql.sendinsert("DELETE FROM mapping WHERE username = '%s'" % userrow["name"])
		mysql.closeconnection()

#-----------------------Group Methods-------------------------

	def checkGroupExists(self, groupname, host, hostuser, hostpass):
		cmd = 'sudo cat /etc/group | grep %s' % groupname
		shell = shellconnect.shellconnect(host, hostuser, hostpass)
		output = shell.sendcommand(cmd)
		if output == "False" or output == "":
			return False
		else:
			return True

	def createGroupOnHost(self, grouprow, hostrow):
		cmd = 'sudo groupadd %s' % grouprow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def deleteGroupOnHost(self, grouprow, hostrow):
		cmd = 'sudo delgroup %s' % grouprow["name"]
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def removeGroupFromUser(self, hostrow, row):
		cmd = 'sudo deluser %s %s' % (row["username"], row["groupname"])
		shell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
		shell.sendcommand(cmd)

	def getMappingResultFromGroup(self, grouprow):
		mysql = mysqlconnect.mysqlconnect()
		result = mysql.sendquery("SELECT * FROM mapping WHERE groupname = '%s'" % grouprow["name"])
		mysql.closeconnection()
		return result

	def deleteGroupAndMappingInDB(self, grouprow):
		mysql = mysqlconnect.mysqlconnect()
		mysql.sendinsert("DELETE FROM groups WHERE name = '%s'" % grouprow["name"])
		mysql.sendinsert("DELETE FROM mapping WHERE groupname = '%s'" % grouprow["name"])
		mysql.closeconnection()

#-----------------------Hosts Methods-------------------------

	def checkHostIsUp(self, hostname, username, password):
		shell = shellconnect.shellconnect(hostname, username, password)
		result = shell.sendcommand('echo "test"')
		if result == "False":
			return False
		else:
			return True

	def startHost(self, hostname):
		try:
			cmd = "virsh start "+hostname
			subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
			self.writeStatusMessage(hostname+" init start...")
			self.bootDelay = True
		except subprocess.CalledProcessError as err:
			self.writeStatusMessage(hostname+" already running...")

#-----------------------General Methods-------------------------

	def synchronize(self):
		self.writeStatusMessage("Synchronization started.")
		self.countActions()
		self.synchronizeHosts()
		self.writeStatusMessage("Synchronization finished.")
		time.sleep(1)
		self.resetStatusMessage()
		time.sleep(1)
		self.resetProgress()

	def execute(self, cmd):
		mysql = mysqlconnect.mysqlconnect()
		hostresult = mysql.getAllHosts()
		mysql.closeconnection()

		for hostrow in tqdm((hostresult), ncols=80, desc="Executing"):
			hostOnline = self.checkHostIsUp(hostrow["IP"], hostrow["user"], hostrow["pass"])
			if hostOnline == True:
				self.writeStatusMessage("Execute Command on: " + hostrow["IP"])
				execshell = shellconnect.shellconnect(hostrow["IP"], hostrow["user"], hostrow["pass"])
				execshell.sendcommand(cmd)
			else:
				self.writeStatusMessage("Host %s is down... Skip to next host." % hostrow["IP"])

#-----------------------Helper Methods-------------------------

	def countActions(self):
		mysql = mysqlconnect.mysqlconnect()
		user = mysql.countrow("SELECT * FROM user")
		groups = mysql.countrow("SELECT * FROM groups")
		hosts = mysql.countrow("SELECT * FROM remotehosts")
		mysql.closeconnection()
		result = (user+groups)*hosts
		self.actionTotal = result

	def checkProgress(self):
		load = open(settings.ROOT_PATH+"progress", "r")
		progress = load.read(3)
		load.close()
		print(progress)

	def writeProgress(self, counter):
		percent = (float(counter)/float(self.actionTotal))*100
		percent = int(round(percent))
		load = open(settings.ROOT_PATH+"progress", "w")
		load.write(str(percent))
		load.close()

	def resetProgress(self):
		load = open(settings.ROOT_PATH+"progress", "w")
		load.write("0")
		load.close()

	def checkStatusMessage(self):
		load = open(settings.ROOT_PATH+"statusmessage", "r")
		message = load.read(100)
		load.close()
		print(message)

	def writeStatusMessage(self, message):
		print(message)
		load = open(settings.ROOT_PATH+"statusmessage", "w")
		load.write(message)
		load.close()

	def resetStatusMessage(self):
		load = open(settings.ROOT_PATH+"statusmessage", "w")
		load.write("")
		load.close()