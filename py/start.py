#!/usr/bin/python2.7
from logger import Logger
import deploy
import remotehosts
import user
import groups
import test
from sys import argv

class start():

	def __init__(self):
		self.logger = Logger(self.__class__.__name__).get()
		size = len(argv)
		if size <= 1:
			self.logger.error("No Parameters found. Check [--help] for details.")
			exit()

		if "--help" in argv:
			print("=============GENERAL=====================================================================")
			print("--help                              | This help text.")
			print("--synchronize                       | Synchronize all Users and Groups on all Hosts.")
			print("--execute [\"Command\"]               | Execute a command on all Hosts.")
			print("--run-tests                         | Run all health tests.")
			print("\n=============USER========================================================================")
			print("--create-user [user]                | Create a new User.")
			print("--change-password [user] [pass]     | Changes the Password of a User.")
			print("--change-name [olduser] [newuser]   | Changes the Name of an existing User.")
			print("--remove-user [user]                | Deletes an existing User.")
			print("--disable-user [user]               | Lock an User Account.")
			print("--enable-user [user]                | Unlock an User Account.")
			print("\n=============GROUPS======================================================================")
			print("--create-group [group]                  | Create a new Group.")
			print("--remove-group [group]                  | Deletes an existing Group.")
			print("--add-user-to-group [user] [group]      | Add an existing User to an existing Group.")
			print("--remove-user-from-group [user] [group] | Remove an existing User from an existing Group.")
			print("\n=============HOSTS=======================================================================")
			print("--add-host [name] [ip] [user] [pass]    | Add a new Hosts.")
			print("--remove-host [ip]                      | Remove an existing host.")
			print("--start-host [hostname]                 | Sends a Magic Packet to a Host.")
			exit()

		if "--check-status-message" in argv:
			try:
				obj = deploy.deploy()
				deploy.deploy.checkStatusMessage(obj)
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--check-progress" in argv:
			try:
				obj = deploy.deploy()
				deploy.deploy.checkProgress(obj)
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--check-host-is-up" in argv:
			try:
				obj = deploy.deploy()
				deploy.deploy.checkHostIsUp(obj, argv[2], argv[3], argv[4])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--start-host" in argv:
			try:
				obj = deploy.deploy()
				deploy.deploy.startHost(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--synchronize" in argv:
			try:
				obj = deploy.deploy()
				deploy.deploy.synchronize(obj)
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--execute" in argv:
			try:
				size = len(argv)
				iterator = 2
				cmd = ""
				while size>iterator:
					cmd = str(cmd)+str(argv[iterator])
					iterator = iterator + 1

				obj = deploy.deploy()
				deploy.deploy.execute(obj, cmd)
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--create-user" in argv:
			try:
				obj = user.user()
				user.user.createuser(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--change-password" in argv:
			try:
				obj = user.user()
				user.user.changepassword(obj, argv[2], argv[3])
			except IndexError:
				self.logger.error("Invalid Syntax of Arguments. Check [--help] for details.")
			exit()

		if "--change-name" in argv:
			try:
				obj = user.user()
				user.user.changename(obj, argv[2], argv[3])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--remove-user" in argv:
			try:
				obj = user.user()
				user.user.removeuser(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--disable-user" in argv:
			try:
				obj = user.user()
				user.user.disableuser(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--enable-user" in argv:
			try:
				obj = user.user()
				user.user.enableuser(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--create-group" in argv:
			try:
				obj = groups.groups()
				groups.groups.creategroup(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--remove-group" in argv:
			try:
				obj = groups.groups()
				groups.groups.removegroup(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--add-user-to-group" in argv:
			try:
				obj = groups.groups()
				groups.groups.addusertogroup(obj, argv[2], argv[3])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--remove-user-from-group" in argv:
			try:
				obj = groups.groups()
				groups.groups.removeuserfromgroup(obj, argv[2], argv[3])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--add-host" in argv:
			try:
				obj = remotehosts.remotehosts()
				remotehosts.remotehosts.addhost(obj, argv[2], argv[3], argv[4], argv[5])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--remove-host" in argv:
			try:
				obj = remotehosts.remotehosts()
				remotehosts.remotehosts.removehost(obj, argv[2])
			except IndexError:
				self.logger.error("Invalid Syntax in Arguments. Check [--help] for details.")
			exit()

		if "--run-tests" in argv:
			obj = test.test()
			test.test.run_all(obj)
			exit()

start()