<?php

if(isset($_GET["s"])){
	exec("/var/www/html/usermgmt/py/start.py --synchronize", $output);
	header('Location: index.php?sync');
}

if(isset($_GET["e"])){
	exec("/var/www/html/usermgmt/py/start.py --execute \"".$_POST["cmd"]."\"", $output);
	header('Location: index.php?executed');
}

if(isset($_GET["on"])){
	exec("/var/www/html/usermgmt/py/start.py --start-host \"".$_GET["name"]."\"", $output);
	header('Location: index.php');
}

if(isset($_GET["u"])){
	switch ($_GET["u"]){
		case 1:
			exec("/var/www/html/usermgmt/py/start.py --create-user ".$_POST["username"], $output);
			header('Location: index.php');
			break;
		case 2:
			exec("/var/www/html/usermgmt/py/start.py --change-password ".$_POST["username"]." ".$_POST["password"], $output);
			header('Location: index.php');
			break;
		case 3:
			exec("/var/www/html/usermgmt/py/start.py --change-name ".$_POST["username"]." ".$_POST["newusername"], $output);
			header('Location: index.php');
			break;
		case 4:
			exec("/var/www/html/usermgmt/py/start.py --remove-user ".$_POST["username"], $output);
			header('Location: index.php');
			break;
		case 5:
			exec("/var/www/html/usermgmt/py/start.py --disable-user ".$_POST["username"], $output);
			header('Location: index.php');
			break;
		case 6:
			exec("/var/www/html/usermgmt/py/start.py --enable-user ".$_POST["username"], $output);
			header('Location: index.php');
			break;
	}

}

if(isset($_GET["g"])){
	switch ($_GET["g"]){
		case 1:
			exec("/var/www/html/usermgmt/py/start.py --create-group ".$_POST["groupname"], $output);
			header('Location: index.php');
			break;
		case 2:
			exec("/var/www/html/usermgmt/py/start.py --remove-group ".$_POST["groupname"], $output);
			header('Location: index.php');
			break;
		case 3:
			exec("/var/www/html/usermgmt/py/start.py --add-user-to-group ".$_POST["username"]." ".$_POST["groupname"], $output);
			header('Location: index.php');
			break;
		case 4:
			exec("/var/www/html/usermgmt/py/start.py --remove-user-from-group ".$_POST["username"]." ".$_POST["groupname"], $output);
			header('Location: index.php');
			break;
	}

}

if(isset($_GET["h"])){
	switch ($_GET["h"]){
		case 1:
			exec("/var/www/html/usermgmt/py/start.py --add-host ".$_POST["hostname"]." ".$_POST["ip"]." ".$_POST["username"]." ".$_POST["password"], $output);
			header('Location: index.php');
			break;
		case 2:
			exec("/var/www/html/usermgmt/py/start.py --remove-host ".$_POST["ip"], $output);
			header('Location: index.php');
			break;
	}

}

?>