#!/usr/bin/python2.7
ROOT_PATH = '/var/www/html/usermgmt/'
LOGGING_DIR = 'logs/'

DEFAULT_PASSWD = '1234QWer'
CONTROLLER_IP = '192.168.122.1'
BOOT_DELAY = 30

DB_HOSTNAME = 'localhost'
DB_USERNAME = 'root'
DB_PASSWORD = '1234QWer'
DB_PORT = 3306
DB_DATABASE = 'usermgmt'