<html>
<head>
<title>User-Management</title>
<link href="/style/bootstrap.css" rel="stylesheet" />
<link href="/style/customstyle.css" rel="stylesheet" />
<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
<script language="javascript" type="text/javascript" src="/script/ajax.js"></script>

<?php

include ("sql.php");

?>
</head>
<body>

<div id="overlay">
<div id="statusmessage"></div>
<div id="outer_bar"><div id="inner_bar"></div></div>
<div id="spinner" class="la-line-scale la-3x">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">User-Management</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
<?php if($_GET["p"] == 0){echo "<li class=\"nav-item active\">";}?>
        <a class="nav-link" onclick="init(false)" href="index.php?p=0">Overview</a>
      </li>
<?php if($_GET["p"] == 1){echo "<li class=\"nav-item active\">";}?>
        <a class="nav-link" href="index.php?p=1">Users</a>
      </li>
<?php if($_GET["p"] == 2){echo "<li class=\"nav-item active\">";}?>
        <a class="nav-link" href="index.php?p=2">Groups</a>
      </li>
<?php if($_GET["p"] == 3){echo "<li class=\"nav-item active\">";}?>
        <a class="nav-link" href="index.php?p=3">Remotehosts</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="api.php?e" method="post">
      <input name="cmd" class="form-control mr-sm-2" type="text" placeholder="Send Command">
      <button onclick="init(false);" class="btn btn-secondary my-2 my-sm-0" type="submit">Execute on all hosts</button>
    </form>
  </div>
</nav>

<div id="leftside">

<?php
include("health.php");
?>

<div class="card text-white bg-info mb-3" style="max-width: 20rem;">
  <div class="card-header">Information</div>
  <div class="card-body">
    <h4 class="card-title">Synchronizing Process</h4>
    <p class="card-text">
    	Please keep in mind, that the info shown may isn't up to date with all remote systems.
    	Perform a Deployment to syncronize with all remotehosts.
    </p>
  </div>
</div>

<div class="card border-info mb-3" style="max-width: 20rem;">
  <div class="card-header">Synchronizing</div>
  <div class="card-body">
    <h4 class="card-title">Start Synchronizing</h4>
        <p class="card-text">This process may take some time.<br>(2m-5m)<br>
      <form action="api.php?s" method="post"><button onclick="init(true); ajaxloop();" type="submit" class="btn btn-info">SYNCHRONIZE</button></form>
    </p>
  </div>
</div>

<ul class="list-group" style="margin-bottom: 20px !important;">
  <li class="list-group-item d-flex justify-content-between align-items-center" style="border: 1px solid red;">
    Users to delete by next deployment.
    <span class="badge badge-primary badge-pill">
<?php
$query = "SELECT COUNT(ID) FROM user WHERE state = 'deleted' LIMIT 1;";
$result = $mysqli->query ($query);                
while($row = $result->fetch_row()){
	echo $row[0];
}
?>
    </span>
  </li>
  <li class="list-group-item d-flex justify-content-between align-items-center" style="border: 1px solid orange;">
    Users will be locked by next deployment.
    <span class="badge badge-primary badge-pill">
<?php
$query = "SELECT COUNT(ID) FROM user WHERE state = 'locked' LIMIT 1;";
$result = $mysqli->query ($query);                
while($row = $result->fetch_row()){
	echo $row[0];
}
?>
    </span>
  </li>
  <li class="list-group-item d-flex justify-content-between align-items-center" style="border: 1px solid green;">
    Users Total
    <span class="badge badge-primary badge-pill">
<?php
$query = "SELECT COUNT(ID) FROM user LIMIT 1;";
$result = $mysqli->query ($query);                
while($row = $result->fetch_row()){
	echo $row[0];
}
?>
    </span>
  </li>
</ul>

</div>


<?php

if(isset($_GET["p"])){
switch($_GET["p"]){
	case 0:
		include("pages/overview.php");
		break;
	case 1:
		include("pages/user.php");
		break;
	case 2:
		include("pages/groups.php");
		break;
	case 3:
		include("pages/hosts.php");
		break;
	default:
		include("pages/overview.php");
		break;
}
}
else{
	include("pages/overview.php");
}

?>

</body>
</html>
