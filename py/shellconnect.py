#!/usr/bin/python2.7
from logger import Logger
import spur
import warnings

class shellconnect(object):

    def __init__(self, hostname, username, password):
        warnings.filterwarnings(action='ignore',module='.*paramiko.*')

        self.logger = Logger(self.__class__.__name__).get()
        self.hostname = hostname
        self.username = username
        self.password = password

    def sendcommand(self, command):
        try:
            shell = spur.SshShell(
                hostname=self.hostname,
                username=self.username,
                password=self.password,
                missing_host_key=spur.ssh.MissingHostKey.accept)

            with shell:
                result = shell.run(["sh", "-c", command], allow_error=True)
                return(result.output)

        except spur.ssh.ConnectionError:
            self.logger.error("Unable to connect via SSH on host: %s" % self.hostname)
            print("False")
