#!/usr/bin/python2.7
import mysqlconnect
import user
import groups
import remotehosts
import deploy
from logger import Logger
import settings

class test():

	def __init__(self):
		self.logger = Logger(self.__class__.__name__).get()

		self.health  = 0

	def run_all(self):
		self.test_user("testdummy")
		self.test_group("testdummy")
		self.test_remotehost("testdummy", "1.1.1.1", "root", "1234QWer")
		self.test_progress(42)
		self.test_database()
		self.write_health()
		exit()

	def write_health(self):
		load = open(settings.ROOT_PATH+"health", "w")
		load.write(str(self.health))
		load.close()

	def test_user(self, username):
		obj = user.user()
		user.user.createuser(obj, username)
		result = user.user.checkuserexists(obj, username)
		if (result):
			print("\033[94m\033[4mTEST: SUCCESS + (User System is working.)\033[0m\n")
			self.health = self.health + 1
		else:
			print("\033[91m\033[4mTEST: FAILED  - (User System not working.)\033[0m\n")
		mysql = mysqlconnect.mysqlconnect()
		mysql.sendinsert("DELETE FROM user WHERE name = '%s'" % username)
		mysql.closeconnection()

	def test_group(self, groupname):
		obj = groups.groups()
		groups.groups.creategroup(obj, groupname)
		result = groups.groups.checkgroupexists(obj, groupname)
		if (result):
			print("\033[94m\033[4mTEST: SUCCESS + (Group System is working.)\033[0m\n")
			self.health = self.health + 1
		else:
			print("\033[91m\033[4mTEST: FAILED  - (Group System not working.)\033[0m\n")
		mysql = mysqlconnect.mysqlconnect()
		mysql.sendinsert("DELETE FROM groups WHERE name = '%s'" % groupname)
		mysql.closeconnection()

	def test_remotehost(self, hostname, IP, username, password):
		obj = remotehosts.remotehosts()
		remotehosts.remotehosts.addhost(obj, hostname, IP, username, password)
		result = remotehosts.remotehosts.checkhostname(obj, hostname)
		if (result):
			print("\033[94m\033[4mTEST: SUCCESS + (Host System is working.)\033[0m\n")
			self.health = self.health + 1
		else:
			print("\033[91m\033[4mTEST: FAILED  - (Host System not working.)\033[0m\n")
		mysql = mysqlconnect.mysqlconnect()
		mysql.sendinsert("DELETE FROM remotehosts WHERE name = '%s'" % hostname)
		mysql.closeconnection()

	def test_progress(self, value):
		obj = deploy.deploy()
		deploy.deploy.countActions(obj)
		deploy.deploy.writeProgress(obj, value)
		output = deploy.deploy.checkProgress(obj)
		if (output != 0):
			print("\033[94m\033[4mTEST: SUCCESS + (Progress System is working.)\033[0m\n")
			self.health = self.health + 1
		else:
			print("\033[91m\033[4mTEST: FAILED  - (Progress System not working.)\033[0m\n")
		deploy.deploy.resetProgress(obj)

	def test_database(self):
		mysql = mysqlconnect.mysqlconnect()
		hostsize = mysql.countrow("SELECT * FROM remotehosts")
		if (hostsize != 0):
			print("\033[94m\033[4mTEST: SUCCESS + (Database is working.)\033[0m\n")
			self.health = self.health + 1
		else:
			print("\033[91m\033[4mTEST: FAILED  - (Database not working.)\033[0m\n")
		mysql.closeconnection()

