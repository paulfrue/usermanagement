#!/usr/bin/python2.7
import mysqlconnect
from logger import Logger
import settings

class user():

    def __init__(self):
        self.logger = Logger(self.__class__.__name__).get()

    def checkuserexists(self, username):
        mysql = mysqlconnect.mysqlconnect()
        result = mysql.sendquery("SELECT * FROM user WHERE name = '%s'" % username)
        mysql.closeconnection()
        if result:
            return True
        else:
            return False

    def createuser(self, username):
        if self.checkuserexists(username):
            self.logger.error("User -> " + username + " already exists.")
        else:
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("INSERT INTO user (ID,name,state,pass) VALUES (0, '%s', 'active', '%s')" % (username, settings.DEFAULT_PASSWD))
            mysql.closeconnection()
            self.logger.info("Creating User -> " + username)

    def changepassword(self, username, password):
        if self.checkuserexists(username):
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("UPDATE user SET pass = '%s' WHERE name = '%s'" % (password, username))
            mysql.closeconnection()
            self.logger.info("Changing Password of User -> " + username)
        else:
            self.logger.error("User -> " + username + " doesn't exist.")

    def changename(self, usernameold, usernamenew):
        if self.checkuserexists(usernameold):
            if self.checkuserexists(usernamenew):
                self.logger.error("New User -> " + usernamenew + " already exists.")
            else:
                mysql = mysqlconnect.mysqlconnect()
                mysql.sendinsert("UPDATE user SET name = '%s' WHERE name = '%s'" % (usernamenew, usernameold))
                mysql.sendinsert("INSERT INTO user (ID,name,state,pass) VALUES (0, '%s', 'locked', '%s')" % (usernameold, settings.DEFAULT_PASSWD))
                mysql.closeconnection()
                self.logger.info("Changing Name of User -> " + usernameold + " to " + usernamenew)
        else:
            self.logger.error("Old User -> " + usernameold + " doesn't exist.")

    def removeuser(self, username):
        if self.checkuserexists(username):
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("UPDATE user SET state = 'deleted' WHERE name = '%s'" % username)
            mysql.closeconnection()
            self.logger.info("Removing User -> " + username)
        else:
            self.logger.error("User -> " + username + " doesn't exist.")

    def disableuser(self, username):
        if self.checkuserexists(username):
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("UPDATE user SET state = 'locked' WHERE name = '%s'" %username)
            mysql.closeconnection()
            self.logger.info("Disable User -> " + username)
        else:
            self.logger.error("User -> " + username + " doesn't exist.")

    def enableuser(self, username):
        if self.checkuserexists(username):
            mysql = mysqlconnect.mysqlconnect()
            mysql.sendinsert("UPDATE user SET state = 'active' WHERE name = '%s'" % username)
            mysql.closeconnection()
            self.logger.info("Enable User -> " + username)
        else:
            self.logger.error("User -> " + username + " doesn't exist.")